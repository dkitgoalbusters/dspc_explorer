<%-- 
    Document   : ModifyUser
    Created on : Apr-17-2018, 08:17:13
    Author     : Abdul
--%>
<script>
    setCaptcha(document.getElementById('signupform'));
</script>
<div class="modal-content container">
    <div class="col-md-12">
        <!-- Modal content-->
        <div >
            <div class="modal-header">
                <button type="button" class="close" onclick="closeAddUserModal()">&times;</button>
                <!--// method defined in ManageUsers.jsp-->
                <center> <h4 class="modal-title" ><B>Add User</b></h4></center>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="signupbox" class="col-lg-12">
                        <form id="signupform" role="form" method="POST" onsubmit="addUser(this); return false;"><!--method defined in ManageUsers.jsp-->
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="userName" placeholder="Email Address" required>
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" name="userPassword" placeholder="Password" 
                                       required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" 
                                       onchange="this.setCustomValidity(this.validity.patternMismatch ?
                                                       'Password must contain at least 6 characters, including UPPER/lowercase and numbers' : '');
                                               if (this.checkValidity())
                                                   form.confirmPassword.pattern = this.value;">
                            </div>
                            <div class="form-group">
                                <label for="password">Confirm Password</label>
                                <input type="password" class="form-control" name="confirmPassword" placeholder="Password" 
                                       required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" 
                                       onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Please enter the same Password as above' : '');">
                            </div>
                            
                            <div class="form-group">
                                <label for="text">Account Type</label>
                                <input type="text" class="form-control" name="usertype" placeholder="gravedigger,registrar">
                            </div>
                            <input type="hidden" name="action" value="addUsers">
                            <div class="form-group">
                                <!-- Button -->                                        
                                <button type="submit" class="btn btn-default">Add Account</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer ">

            </div>
        </div>
    </div>
</div>
