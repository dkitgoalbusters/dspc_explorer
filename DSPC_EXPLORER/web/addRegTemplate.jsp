<%-- 
    Document   : addRegTemplate
    Created on : 25-May-2018, 01:27:05
    Author     : abdul
--%>

<div class="modal-content container">
    <div class="col-md-12">
        <!-- Modal content-->
        <div >
            <div class="modal-header">
                <button type="button" class="close" onclick="closeAddUserModal()">&times;</button>
                <!--// method defined in ManageUsers.jsp-->
                <center> <h4 class="modal-title" ><B>Add New Entry To Records</b></h4></center>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="signupbox" class="col-lg-12">
                        <form id="signupform" role="form" method="POST" onsubmit="addReg(this);"><!--method defined in ManageUsers.jsp-->
                            <div class="form-group ">
                                <label for="firstname">Grave Number</label>
                                <input type="text" class="form-control" name="graveid" placeholder="grave number" required>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="firstname">First Name</label>
                                <input type="text" class="form-control" name="firstname" placeholder="First Name" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="Middlename">Middle Name</label>
                                <input type="text" class="form-control" name="middlename" placeholder="Enter Middle name(Optional)">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="Lastname">Last Name</label>
                                <input type="text" class="form-control" name="lastname" placeholder="Enter Last Name">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="sex">Sex</label>
                                <input type="text" class="form-control" name="sex" placeholder=" Male or Female ">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="age">Age</label>
                                <input type="text" class="form-control" name="age" placeholder="Enter age">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="depth">Religion</label>
                                <input type="text" class="form-control" name="religion" placeholder="Enter religion">
                            </div>

                            <div class="form-group col-md-12">
                                <label for="occupation">Occupation</label>
                                <input type="text" class="form-control" name="occupation" placeholder="Enter occupation (optional)">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="occupation">Marriage Status</label>
                                <input type="text" class="form-control" name="marStat" placeholder="Enter marriage status (optional)">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="depth">Death Location</label>
                                <input type="text" class="form-control" name="dlocation" placeholder="Enter a location of death ">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="bdate">Death Date</label>
                                <input type="text" class="form-control" name="ddate" placeholder="Format (YYYY-MM-DD) e.g (2011-01-30) ">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="bdate">Burial Date</label>
                                <input type="text" class="form-control" name="bdate" placeholder="Format (YYYY-MM-DD) e.g (2011-01-30) ">
                            </div>

                            <input type="hidden" name="action" value="addRegistrar">
                            <div class="form-group">
                                <!-- Button -->                                        
                                <button type="submit" class="btn btn-default">Add Record</button>
                            </div> 
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer ">

            </div>
        </div>
    </div>
</div>




