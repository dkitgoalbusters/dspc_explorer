<%-- 
    Document   : HomeTest
    Created on : 16-Apr-2018, 20:27:52
    Author     : Asare
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<style>


    header.masthead {
        position: relative;
        background: url("images/Dundalk-Graveyard.jpg") no-repeat center center;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        color:white;
        padding-top: 8rem;
        padding-bottom: 8rem;
    }


    section.features-icons{
        background-color: white;
    }
    .features-icons {
        padding-top: 7rem;
        padding-bottom: 7rem;
        padding-left: 7rem;
    }

    .features-icons .features-icons-item .features-icons-icon {
        height: 2rem;
        border: 1px solid red;
        max-width: 17rem;
        max-height: 15rem;
    }

    .features-icons .features-icons-item .features-icons-icon i {
        font-size: 4.5rem;
    }

    .features-icons .features-icons-item:hover .features-icons-icon i {
        font-size: 5rem;
    }
    section.promotion{
        position: relative;
        background: url("images/DSPC-Image3.jpg") no-repeat center center;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        color:white;
        padding-top: 8rem;
        padding-bottom: 8rem;

    }

    section.promotion .header-content {
        margin-bottom: 0;
        text-align: left;
    }
    section.promotion .header-content h1 {
        font-size: 50px;
    }

    section.promotion.device-container {
        max-width: 325px;
        margin-right: auto;
        margin-left: auto;
        

    }

    section.people{
        background-color: whitesmoke;
        padding-top: 8rem;
        padding-bottom: 8rem;
    }
    section.people .people-item{
        max-width: 18rem;
    }

    section.people .people-item img{
        max-width: 12rem;
        box-shadow: 0px 5px 5px 0px #adb5bd;
    }
    .hidden
    {
        display:none;
    }
    
@media only screen and (max-width:1000px) {
    div.people-container{
        padding-left: 150px;
        
    }
}

</style>
<script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
<script>

    
    function myFunction(){
        var x = document.getElementById("advance_search_form");
        if(x.style.display === "block") {
            x.style.display = "none";
        }else {
            x.style.display = "block";
        }
    }
</script>

<body>
    <header class="masthead">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2">
                    <h1 style="color: white; "><strong>Welcome to Dowdallshill Saint Patrick Cemetery Dundalk Website!</strong></h1>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <h3 class="text-center"><strong>Enter Details Below</strong></h3>
                        <form action="index.php" id="search-form" method="POST" onsubmit="searchRecords(this); return false;" class="navbar-form navbar-right" role="search" id="simple_search_form">
                            <div class="form-row">
                                
                                <div class="col-md-12 col-sm-12"> 
                                    <div class="row1">
                                    <div class="form-group">
                                        <input type="hidden" name="action" value="searchRecords">
                                    </div>
                                    <div class="col-xl-2  col-lg-2  col-md-2 "></div>
                                    <div class="form-group col-md-3 col-sm-4" >
                                        <label><strong>First Name</strong></label>
                                        <input type="text" class="form-control" name="firstname" placeholder="First name">
                                    </div>
                                    <div class="form-group col-md-3 col-sm-4">
                                        <label><strong>Last Name</strong></label>
                                        <input type="text" class="form-control" name="lastname" placeholder="Last name">
                                    </div>
                                    <div class="form-group col-md-3 col-sm-4">
                                        <label><strong>Death Age</strong></label>
                                        <input type="text" class="form-control" name="ddate" placeholder="Died At Age of...">
                                    </div>
                                </div> 
                                </div>
                                   <div id="advance_search_form" style="display:none;">
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <div class="col-xl-1  col-lg-1  col-md-1"></div>
                                        <div class="form-group col-md-2 col-sm-3">
                                            <label>Birth Date</label>
                                            <input type="text" class="form-control" placeholder="Birth Date">
                                        </div>
                                        <div class="col-xl-1  col-lg-1  col-md-1 col-sm-1"></div>
                                        <div class="form-group col-md-2 col-sm-3">
                                            <label>Death Date</label>
                                            <input type="text" class="form-control" placeholder="Death Date">
                                        </div>
                                        <div class="col-xl-1  col-lg-1  col-md-1 col-sm-1"></div>
                                        <div class="form-group col-md-2 col-sm-3">
                                            <label>Burial Date</label>
                                            <input type="text" class="form-control" placeholder="Burial Date">
                                        </div>
                                        <div class="col-xl-1  col-lg-1  col-md-1 col-sm-1"></div>
                                        <div class="form-group col-md-2 col-sm-2">
                                            <label>Gender:</label>
                                            <select class="form-control">
                                                <option value="1">Male</option>
                                                <option value="2">Female</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-12"> 
                                        <div class="col-xl-2  col-lg-2  col-md-2"></div>
                                        <div class="form-group col-md-2 col-sm-3">
                                            <label>Martial status</label>
                                            <input type="text" class="form-control" placeholder="Marital status">
                                        </div>
                                        <div class="col-xl-1  col-lg-1  col-md-1 col-sm-1"></div>
                                        <div class="form-group col-md-2 col-sm-3">
                                            <label>Occupation</label>
                                            <input type="text" class="form-control" placeholder="Occupation">
                                        </div>
                                        <div class="col-xl-1  col-lg-1  col-md-1 col-sm-1"></div>
                                        <div class="form-group col-md-2 col-sm-4">
                                            <label>Death Location</label>
                                            <input type="text" class="form-control" placeholder="Death Location">
                                        </div>
                                    </div>
                                </div>
                                 </div>
                                <div class="col-md-6 col-sm-8" id="search-box" style="left:200px; top:10px;">
                                <input type="hidden" name="action" value="searchRecords">
                                <button type="submit" class="btn btn-primary">Search</button>

                                <input type="hidden" name="action" value="searchRecords">
                                <button type="button" onclick="myFunction()" class="btn btn-success" id="search_heading">Advance Search</button>
                                </div>
                        </form>
                        <br><br>
                    </div>
                </div>
            </div> 
        </div>
    </header>

    <section class="features-icons">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="features-icons-item"> 

                        <img src="images/search1.png" style=" max-width: 17rem; width: 100%; height: 150px; " alt=""/>
                        <h3><strong>Search</strong></h3>
                        <p class="lead mb-0">Search through our records just simply type in the surname your looking for and we'll do the rest</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                        <img src="images/discover1.png" style="max-width: 17rem; width: 100%; height: 150px; " alt=""/>
                        <h3><strong>Discover</strong></h3>
                        <p class="lead mb-0">The minute you find the person your looking for why don't you look through their family history</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="features-icons-item mx-auto mb-0 mb-lg-3">

                        <img src="images/locate1.png" style=" max-width: 17rem; width: 100%; height: 150px; " alt=""/>
                        <h3><strong>Locate</strong></h3>
                        <p class="lead mb-0">Once your assured why don't you that particular person at Dowdallshil Saint Patrick Cemetery  </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="promotion">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="header-content mx-auto">
                        <h1 class="mb-5">Allow our Dowdallshill Saint Patrick Cemetery App to direct you to the right grave for you. Making research more accessible to everyone</h1>
                        <a href="https://play.google.com/store/apps/details?id=com.DSPC.kieron.dspc_explorer&hl=en"><img src="images/googlePlay.png" style="height: 100px" alt=""/></a>
                    </div>
                </div>
                <div class="col-md-2 col-sm-1">
                    <div class="device-container">
                        <img src="images/dspc-app.png"class="img-fluid"style="height: 600px;" alt=""/>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</section>

<section class="people text-center bg-light">
    <div class="container">
        <h2 class="mb-5"><strong>Our Team</strong></h2>
        <div class="people-container">
        <div class="row">
            
            <div class="col-md-3 col-sm-5">
                <div class="people-item mx-auto mb-5 mb-lg-0">
                    <img class="img-fluid img-circle mb-3" src="images/abdul.png" alt=""/>
                    <h2>Abdul S.</h2>
                    <p class="font-weight-light mb-0">Back-End Developer</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-5">
                <div class="people-item mx-auto mb-5 mb-lg-0">
                    <img class="img-fluid img-circle mb-3" src="images/David.A.png" alt=""/>                  
                    <h2>David A.</h2>
                    <p class="font-weight-light mb-0">Lead Tester</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-5">
                <div class="people-item mx-auto mb-5 mb-lg-0">
                    <img class="img-fluid img-circle mb-3" src="images/vitaliy.png" alt=""/>

                    <h2>Vitaliy V.</h2>
                    <p class="font-weight-light mb-0">Front-End Developer</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-5">
                <div class="people-item mx-auto mb-5 mb-lg-0">
                    <img class="img-fluid img-circle mb-3" src="images/kieron.jpg" alt=""/>
                    <h2>Kieron P.</h2>
                    <p class="font-weight-light mb-0">Mobile Developer</p>
                </div>
            </div>
        </div>
        </div>
    </div>

</div>
</div>
</section>

</body>

