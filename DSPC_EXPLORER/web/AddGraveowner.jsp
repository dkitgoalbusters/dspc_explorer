<%-- 
    Document   : AddGraveowner
    Created on : 20-Feb-2018, 2:11:06
    Author     : Abdul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><b>Add New Grave Owner to Section</b></h3>
            </div>
            <div class="modal-body">
                <form id="signupform" role="form" method="POST" onsubmit="graveOwner(this);"><!--method defined in ManageUsers.jsp-->

                    <div class="form-group">
                        <label for="text">Section</label>
                        <input type="text" class="form-control" name="id" value="1">
                    </div>

                    <div class="form-group">
                        <label for="text">Grave Reference Code</label>
                        <input type="text" class="form-control" name="graverefcode" placeholder="Enter Reference Code" required>
                    </div>
                    <div class="form-group">
                        <label for="graveowner">Full Name</label>
                        <input type="text" class="form-control" name="name" placeholder="Grave Owner Name..">
                    </div>
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" class="form-control" name="address" placeholder="Grave Owner Address">
                    </div>

                    <div class="form-group">
                        <label for="dateopened">Date Opened</label>
                        <input type="text" class="form-control" name="dateopened" placeholder="Date Opened">
                    </div>

                    <div class="form-group">
                        <label for="row">Grave Row</label>
                        <input type="text" class="form-control" name="row" placeholder="Enter The row ">
                    </div>

                    <div class="form-group">
                        <label for="gsize">Grave Size</label>
                        <input type="text" class="form-control" name="gsizes" placeholder="Enter eg. Singl or Double ">
                    </div>

                    <div class="form-group">
                        <label for="depth">Grave Depth</label>
                        <input type="text" class="form-control" name="depth" placeholder="Enter The depth ">
                    </div>

                    <input type="hidden" name="action" value="addGraveOwner">
                    <div class="form-group">
                        <!-- Button -->                                        
                        <button type="submit" class="btn btn-default">Add Grave Owner</button>
                    </div>
                </form>
            </div>
           
        </div>

    </div>


 <a onclick="manageGraveOwners();"><h1><b>Click Here to return... View Records</b></h1> </a>