<%-- 
    Document   : ManageUsers
    Created on : 16-Feb-2018, 21:08:17
    Author     : Abdul
--%>

<%@page import="com.dspc_explorer.Dtos.Registrar"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script src="css/jquery-1.12.4.js"></script>
<script src="css/jquery.dataTables.min.js"></script>
<link href="css/jquery.dataTables.min.css" rel="stylesheet">

<script>

    $(document).ready(function () {
        $('#example').DataTable({
            "pagingType": "full_numbers"
        });
    });
</script>
<% String data = (String) request.getSession().getAttribute("jsonStringUserList");
    List<Registrar> list = (List<Registrar>) request.getSession().getAttribute("list");
%>

<script>
    var registrarList = <%=data%>;
    var selectedUserIds = [];
    storeHtmlToVariable("userBlockTemplate.html", null);
    tmpUserList = [];
    //displayUserList(registrarList);


    function addUser(form)
    {
        registerUser(form);
        closeAddUserModal();
        manageUsers();

    }



</script>

<script>

    $('#loadBtn').on('click', function () {
        alert(<%=data%>);
        var jsonData = JSON.parse(<%=data%>);

        for (var i = 0; i < jsonData.length; i++) {
            var jsonObj = jsonData[i];
            var $tr = $("<tr>");
            var $firstName = $("<td>");
            var $lastName = $("<td>");
            var $sex = $("<td>");
            var $deathlocation = $("<td>");
            var $deathdate = $("<td>");
            var $burialdate = $("<td>");
            $firstName.append(jsonData.Registrar.regFirstName);
            $lastName.append(jsonObj.Registrar.regLastName);
            $sex.append(jsonObj.Registrar.regSex);
            $deathlocation.append(jsonObj.Registrar.regDeathLocation);
            $deathdate.append(jsonObj.Registrar.regDeathDate);
            $burialdate.append(jsonObj.Registrar.regBurialDate);
            $tr.append($firstName);
            $tr.append($lastName);
            $tr.append($sex);
            $tr.append($deathlocation);
            $tr.append($deathdate);
            $tr.append($burialdate);
            $('#tableBody').append($tr);
        }
    });


    function deleteRegistrar(userId)
    {
        deleteReg(userId);
        manageRegistrar();
    }


</script>


<div class="container">
    <div class="col-lg-12">
        <div class="box">
            <div class="row">

                <%if (list.size() > 0 && list != null) { %>   
                <% for (Registrar reg : list) {
                        int userId = reg.getRegId();
                        int deleteId = reg.getRegId();
                %>
                <div id="userContainer">

                    <!--------------------------------------------------------------------------------------------------------------------------------------------------->  
                    <div class="col-md-4">
                        <ul class="nav">
                            <li><a class="borderline">
                                    <div id="userIdDiv" onclick="viewRegistrar(<%=userId%>)">
                                        <div>
                                            <div class="form-group text-center">
                                                <div id="imgcontainer" class="profileimage">
                                                    <img id="img" class="img-thumbnail" alt="Profile Picture" src="images/user.jpg">
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 tinytext">

                                                    <label id="name" class="control-label"></label>
                                                </div>
                                                <div class="col-xs-12 tinytext">
                                                    <label id="username" class="control-label"></label>

                                                </div>
                                            </div>
                                            <div class="margin10">
                                                <div class="row">
                                                    <div class="col-md-12" align="center"> 
                                                        <p>Name:        <%=reg.getRegFirstName()%> <%=reg.getRegLastName()%></p> 
                                                        <p>Death Date:  <%=reg.getRegdeathDate()%></p>  

                                                    </div>
                                                    <div class="col-md-12" align="center"> 
                                                        <button type="button" class="btn btn-link" onclick="viewRegistrar(<%=userId%>)">View</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>



                    <!---------------------------------------------------------------------------------------------------------------------------------------------------> 
                </div>



                <%}
                    }%>






                <button type="button" class="btn btn-link btn-lg" onclick="loadFile('HomeTest.jsp', this);">Return Home</button>


            </div>
        </div>
    </div>



    <div class="col-lg-12">
        <div class="box">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <input id="Special" type="text" class="form-control" oninput="searchUsers(this.value)" placeholder="Search">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-3">
                </div>

            </div>
        </div>
    </div>


</div>


