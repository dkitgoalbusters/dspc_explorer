<%-- 
    Document   : ModifyUser
    Created on : 16-Apr-2018, 21:08:17
    Author     : Abdul
--%>
<script>
    if (activeUser.profileImage != null)
    {
        $("#img").attr("src", "./img/profilePictures/" + activeUser.profileImage);

    } else {
        $("#img").attr("src", "./img/user.jpg");
    }
    $("#fname").val(activeUser.firstName);
    $("#mname").val(activeUser.middleName);
    $("#lname").val(activeUser.lastName);
    $("#uname").val(activeUser.userName);
    $("#Address").val(activeUser.address);
    $("#Town").val(activeUser.town);
    $("#State").val(activeUser.state);
    $("#Country").val(activeUser.country);
    $("#Postcode").val(activeUser.postCode);
    $("#Hnumber").val(activeUser.phone1);
    $("#Mnumber").val(activeUser.phone2);
    $("#idproof").val(activeUser.idtype);
    $("#idref").val(activeUser.idref);
    $("#addprooftype").val(activeUser.addressProofType);
    $("#addref").val(activeUser.addressRef);
    $("[id=userToBeUpdated]").val(activeUser.userId);
    /*function profileImgUpdate(form)
     {
     // name.substr(name.lastIndexOf(".") + 1);
     var fileName = $("#uploadimage").val();
     var extension = fileName.substr(fileName.lastIndexOf(".") + 1);
     activeUser.profileImage = activeUser.userId + "." + extension;
     fileUpload(form);
     loadProfileImage();
     }
     function updateUserProfile(form)
     {
     activeUser.firstName = $("#fname").val();
     activeUser.middleName = $("#mname").val();
     activeUser.lastName=$("#lname").val();
     activeUser.userName$("#uname").val();
     activeUser.address$("#Address").val();
     activeUser.town$("#Town").val();
     activeUser.state$("#State").val();
     activeUser.country$("#Country").val();
     activeUser.postCode$("#Postcode").val();
     activeUser.phone1$("#Hnumber").val();
     activeUser.phone2$("#Mnumber").val();
     activeUser.idtype$("#idproof").val();
     activeUser.idref$("#idref").val();
     activeUser.addressProofType$("#addprooftype").val();
     activeUser.addressRef$("#addref").val();
     activeUser.userId$("[id=userToBeUpdated]").val();
     updateUser(form);
     closeModifyUserModal();
     }*/
</script>
<div class="modal-content container">
    <div class="col-md-12">
        <!-- Modal content-->
        <div >
            <div class="modal-header">
                <button type="button" class="close" onclick="closeModifyUserModal()">&times;</button>
                <center> <h4 class="modal-title" ><B>Modify Users</b></h4></center>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3" onClick="$('#address').hide();
                            $('#proupdate').hide();
                            $('#addressproof').hide();
                            $('#image').show();">     
                        <div class="form-group">
                            <button class="btn btn-primary fulllength" id="">Profile Image</button>
                        </div>
                    </div>
                    <div class="col-md-3" onClick="$('#address').hide();
                            $('#image').hide();
                            $('#addressproof').hide()
                            $('#proupdate').show();">     
                        <div class="form-group">
                            <button class="btn btn-primary fulllength" id="">Personal Information</button>
                        </div>
                    </div>
                    <div class="col-md-3" onClick="$('#proupdate').hide();
                            $('#image').hide();
                            $('#addressproof').hide();
                            $('#address').show();">     
                        <div class="form-group">
                            <button class="btn btn-primary fulllength" id="">Address</button>
                        </div>
                    </div>
                    <div class="col-md-3" onclick="$('#proupdate').hide();
                            $('#image').hide();
                            $('#address').hide();
                            $('#addressproof').show();">     
                        <div class="form-group">
                            <button class="btn btn-primary fulllength" id="">Address Proof</button>
                        </div>
                    </div>
                </div>
                <div id="image">
                    <form role="form" id="FileUploadForm" method="POST" enctype="multipart/form-data" onsubmit="profileImgUpdate(this); return false;">
                        <div class="panel-heading"><h4>Profile Image</h4></div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div id="imgContainer">
                                        <img id="img" src="" width="300px" class="img-thumbnail" alt="Profile Picture" ><br>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input  type="file" multiple class="text-info " name="image" id="uploadimage" accept="image/*" onchange="$('#imgSubmit').removeAttr('disabled')"><br>
                                    <input type="hidden" name="action" value="fileUpload"/>
                                    <input type="hidden" id="userToBeUpdated" name="userToBeUpdated" value=""/>
                                    <input id="imgSubmit" type="submit" disabled class="btn btn-default" value="Upload"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-4">
                                <div class="col-md-6 col-xs-6">
                                    <button type="button" class="btn btn-default fulllength" onclick="closeModifyUserModal()">Close</button>
                                </div>
                                <div class="col-md-6 col-xs-6">
                                    <a onClick="$('#image').hide();
                                            $('#addressproof').hide();
                                            $('#address').hide();
                                            $('#proupdate').show()" class="btn btn-default fulllength">Next</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- *************** Image Upload Part End here *************** -->
                <form role="form" id="updateuser" onsubmit="updateUserProfile(this);
                        closeModifyUserModal();
                        return false;">
                    <div id="proupdate" style="display:none;">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel-heading text-center"><h4><strong>Personal Information</strong></h4></div>
                                <div class="form-group">
                                    <label  for="fname" class="col-md-4 control-label">First Name</label>
                                    <input type="text" class="form-control" name="firstname" id="fname" value=""/>
                                </div>
                                <div class="form-group">
                                    <label  for="mname" class="col-md-4 control-label">Middle Name</label>
                                    <input type="text" class="form-control" name="middlename" id="mname"  value=""/>
                                </div>
                                <div class="form-group">
                                    <label  for="lname" class="col-md-4 control-label">Last Name</label>
                                    <input type="text" class="form-control" name="lastname" id="lname" value=""/>
                                </div>
                                <div class="form-group">
                                    <label  for="username" class="col-md-4 control-label">User Name</label>
                                    <input type="text" class="form-control" name="email" id="uname" disabled value=""/>
                                </div>
                                <div class="form-group text-center">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <button type="button" class="btn btn-default fulllength" onclick="closeModifyUserModal()">Close</button>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <a  onClick="$('#proupdate').hide();
                                            $('#addressproof').hide();
                                            $('#address').hide();
                                            $('#image').show()" class="btn btn-default fulllength">Prev</a>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <a onClick="$('#proupdate').hide();
                                            $('#addressproof').hide();
                                            $('#image').hide();
                                            $('#address').show()" class="btn btn-default fulllength">Next</a>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <!-- end of detail update and start a address update panel -->
                    <div id="address" style="display:none;">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel-heading text-center"><h4><strong>Address</strong></h4></div>
                                <div class="form-group">
                                    <label  for="address" class=" control-label">Address</label>
                                    <input type="text" class="form-control" name="address" id="Address" value=""/>
                                </div>
                                <div class="form-group">
                                    <label  for="Town" class=" control-label">Town</label>
                                    <input type="text" class="form-control" name="town" id="Town" value="" />
                                </div>

                                <div class="form-group">
                                    <label  for="State" class="control-label">State</label>
                                    <input type="text" class="form-control" name="state" id="State" value=""/>
                                </div>

                                <div class="form-group">
                                    <label  for="country" class=" control-label">Country</label>
                                    <input type="text" class="form-control" name="country" id="Country" value=""/>
                                </div>
                                <div class="form-group">
                                    <label  for="Postcode" class=" control-label">Postcode</label>
                                    <input type="text" class="form-control" name="postcode" id="Postcode" value=""/>
                                </div>
                                <div class="form-group">
                                    <label  for="Hnumber" class="control-label">Home Number</label>
                                    <input type="text" class="form-control" name="phone1" id="Hnumber" value=""/>
                                </div>
                                <div class="form-group">
                                    <label  for="Mnumber" class=" control-label">Mobile Number</label>
                                    <input type="text" class="form-control" name="phone2" id="Mnumber" value=""/>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-default fulllength" onclick="closeModifyUserModal()">Close</button>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <a  onClick="$('#image').hide();
                                                    $('#addressproof').hide();
                                                    $('#address').hide();
                                                    $('#proupdate').show()" class="btn btn-default fulllength">Prev</a>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <a onClick="$('#proupdate').hide();
                                                    $('#address').hide();
                                                    $('#addressproof').show()" class="btn btn-default fulllength">Next</a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end of address panel and start a address proof panel -->
                    <div id="addressproof" style="display:none;">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel-heading text-center"><h4><strong>Address Proof</strong></h4></div>
                                <div class="form-group">
                                    <label  for="idproof" class=" control-label">ID Proof</label>
                                    <input type="text" class="form-control" name="idproof" id="idproof" value=""/>
                                </div>
                                <div class="form-group">
                                    <label  for="idref" class=" control-label">Id Reference</label>
                                    <input type="text" class="form-control" name="idref" id="idref" value=""/>
                                </div>
                                <div class="form-group">
                                    <label  for="addprooftype" class=" control-label">Address Proof Type</label>
                                    <input type="text" class="form-control" name="addressprooftype" id="addprooftype" value=""/>
                                </div>
                                <div class="form-group">
                                    <label  for="addref" class=" control-label">Address Reference</label>
                                    <input type="text" class="form-control" name="addressref" id="addref" value=""/>
                                </div>
                                <input type="hidden" name="action" value="update"/>
                                <input type="hidden" id="userToBeUpdated" name="userToBeUpdated" value=""/>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-default fulllength" onclick="closeModifyUserModal()">Close</button>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <a  onClick="$('#proupdate').hide();
                                                    $('#image').hide();
                                                    $('#addressproof').hide();
                                                    $('#address').show()" class="btn btn-default fulllength">Prev</a>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-default fulllength" value="Update"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>                
            </div>
            <div class="modal-footer ">

            </div>
        </div>
    </div>
</div>
