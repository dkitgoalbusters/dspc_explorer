<%-- 
    Document   : ManageUsers
    Created on : 19-Apr-2018, 21:08:17
    Author     : Abdul
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.dspc_explorer.Dtos.Users"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script>
    var selectedUserIds = [];
    userList =<%=request.getSession().getAttribute("jsonStringUserList")%>;
    storeHtmlToVariable("userBlockTemplate.html", null);
    tmpUserList = [];
    displayUserList(userList);
    function displayUserList(tmpUserList)
    {
        selectedUserIds = [];
        $("#userContainer").html("");
        //alert("In Display User");
        for (user in tmpUserList)
        {
            //alert(userList[user].userId+" "+userList[user].profileImage);
            var userId = tmpUserList[user].userId;
            $("#userContainer").append(ajaxResponseText);
            $("#userIdDiv").attr("id", userId);
            if (tmpUserList[user].profileImage == null)
            {
                $("#images").attr("src", "./images/user.jpg");
            } else {
                $("#images").attr("src", "./images/profilePictures/" + tmpUserList[user].profileImage);
            }
            $("#images").attr("id", "images" + userId);
            $("#username").text(tmpUserList[user].userName);
            $("#username").attr("id", "username" + userId);
            var name = "No Name";
            var firstName = $.trim(tmpUserList[user].firstName);
            var lastName = $.trim(tmpUserList[user].lastName);
            if (firstName != null && firstName != "")
            {
                name = firstName;
            }
            if (lastName != null && lastName != "")
            {
                name += " " + lastName;
            }

            $("#name").text(name);
            $("#name").attr("id", "name" + userId);
            if (tmpUserList[user].idVirified === true || tmpUserList[user].idVirified === 'true')
            {
                $("#verified").attr("src", "/images/Verified.jpg");
            }
            $("#verified").attr("id", "verified" + userId);
            $("#modifyButton").val(userId);
            $("#modifyButton").attr("id", userId + "modifyButton");
            $("#chkBox").attr("id", "chkBox" + userId);
        }
    }
    function loadModifyUserTemplate(link, calledFrom)
    {
        clearMessage();
        oldActiveUser = activeUser;
        activeUser = findUserInArray(userList, calledFrom.value);
        $.ajax({
            async: false,
            url: link,
            context: document.body,
            success: function (responseText) {
                $("#modifyUserModal").html(responseText);
            }
        });
    }
    function closeModifyUserModal()
    {
        $('#modifyUserModal').modal('hide');
        if (tmpUserList.length > 0)
        {
            displayUserList(tmpUserList);
        } else
        {
            displayUserList(userList);
        }
        activeUser = oldActiveUser;
    }
    function loadAddUserTemplate(calledFrom)
    {
        clearMessage();
        $.ajax({
            async: false,
            url: "addUserTemplate.jsp",
            context: document.body,
            success: function (responseText) {
                $("#addUserModal").html(responseText);
            }
        });
    }
    function closeAddUserModal()
    {
        $('#addUserModal').modal('hide');
        //alert("closed");
    }
    function addUser(form)
    {
        registerUser(form);
        closeAddUserModal();
        manageUsers();

    }
    function searchUsers(searchString)
    {
        //searchString = searchString.toLowerCase();
        tmpUserList = [];
        var userName;
        var firstName;
        var lastName;
        var found = false;
        if (searchString != null && searchString != "")
        {
            for (user in userList)
            {
                //alert(JSON.stringify(tmpUserList));
                userName = userList[user].userName;
                firstName = userList[user].firstName;
                lastName = userList[user].lastName;
                if (userName != null && userName != "" && userName.search(searchString) > -1)
                {
                    found = true;
                }
                if (firstName != null && firstName != "" && firstName.search(searchString) > -1)
                {
                    found = true;
                }
                if (lastName != null && lastName != "" && lastName.search(searchString) > -1)
                {
                    found = true;
                }
                if (found == true)
                {
                    found = false;
                    tmpUserList.push(userList[user]);
                }

            }
        } else
        {
            tmpUserList = userList;
        }
        displayUserList(tmpUserList);
    }
    function selectUser(userId)
    {
        var chkboxid = "#chkBox" + userId;
        if ($(chkboxid).prop('checked') == true)
        {
            $(chkboxid).prop('checked', false);
            for (id in selectedUserIds)
            {
                if (selectedUserIds[id] == userId)
                {
                    selectedUserIds.splice(id, 1);
                    break;
                }
            }
        } else
        {
            $(chkboxid).prop('checked', true);
            selectedUserIds.push(userId);
        }
        alert(JSON.stringify(selectedUserIds));
    }
</script>
<div class="container">
    <div class="col-lg-12">
        <div class="box">
            <div class="row">
                <div id="userContainer">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="col-lg-12">
        <div class="box">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <input id="Special" type="text" class="form-control" oninput="searchUsers(this.value)" placeholder="Search">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-3">
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <button class="btn btn-primary fulllength" onclick="deleteUser(selectedUserIds)" ><i class="glyphicon glyphicon-trash"> Delete</i></button>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <button class="btn btn-primary fulllength" onclick="loadAddUserTemplate(this);" data-toggle="modal" data-target="#addUserModal"><i class="glyphicon glyphicon-user"> </i>Add User</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal -->   
<div id="modifyUserModal" class="modal fade" role="dialog" dynamic>
</div>
<div id="addUserModal" class="modal fade" role="dialog" dynamic>
</div>