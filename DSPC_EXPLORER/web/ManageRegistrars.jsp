<%-- 
    Document   : ManageRegistrars
    Created on : 16-Feb-2018, 21:08:17
    Author     : Abdul
--%>

<%@page import="com.dspc_explorer.Dtos.SuggestedUpdated"%>
<%@page import="com.dspc_explorer.Dtos.Registrar"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script src="css/jquery-1.12.4.js"></script>
<script src="css/jquery.dataTables.min.js"></script>
<link href="css/jquery.dataTables.min.css" rel="stylesheet">

<script>

    $(document).ready(function () {
        $('#example').DataTable({
            "pagingType": "full_numbers"
        });
    });
</script>
<% String data = (String) request.getSession().getAttribute("jsonStringUserList");
    List<Registrar> list = (List<Registrar>) request.getSession().getAttribute("list");
    List<SuggestedUpdated> updated = (List<SuggestedUpdated>) request.getSession().getAttribute("updates");
%>

<script>
    var registrarList = <%=data%>;
    var selectedUserIds = [];
    storeHtmlToVariable("userBlockTemplate.html", null);
    tmpUserList = [];
    //displayUserList(registrarList);


    function addUser(form)
    {
        registerUser(form);
        closeAddUserModal();
        manageUsers();

    }



</script>

<script>

    function deleteRegistrar(userId)
    {
        deleteReg(userId);
        manageRegistrar();
    }
    
    


</script>



<table id="example" class="display" cellspacing="0" width="100%">

    <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Sex</th>
            <th>Death Location</th>
            <th>Death Date</th>
            <th>Burial Date</th>
            <th>Edit</th>         
            <th>Delete </th>
            <th>View</th>
        </tr>
    </thead>

    <tfoot>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Sex</th>
            <th>Death Location</th>
            <th>Death Date</th>
            <th>Burial Date</th>
            <th>Edit</th>
            <th>Delete </th>
            <th>View</th>

        </tr>
    </tfoot>
    <tbody id="tableBody">
        <%if (list.size() > 0 && list != null) { %>   
        <% for (Registrar reg : list) {
                int userId = reg.getRegId();
                int deleteId = reg.getRegId();
        %>


        <tr> 

            <td><%=reg.getRegFirstName()%></td>
            <td><%=reg.getRegLastName()%></td>
            <td><%=reg.getRegSex()%></td>
            <td><%=reg.getRegDeathLocation()%> </td>
            <td><%=reg.getRegdeathDate()%></td>
            <td><%=reg.getRegburialDate()%></td>
             <td class="text-center"><a href="#"><samp class="glyphicon glyphicon-edit" onclick="modifyRegs(<%=userId%>)"></samp></a></td>
            <td class="text-center"><a href="#"><samp class="glyphicon glyphicon-remove" onclick="deleteRegistrar(<%=deleteId%>)"></samp></a></td>
            <td class="text-center"><a href="#"><samp class="glyphicon glyphicon-eye-open" onclick="viewRegistrar(<%=userId%>)"></samp></a></td>

        </tr>
        <%}
            }%>

    </tbody>
</table>




<!--------------------------------------------------------------------------------------------------------------------------------------------------->







<!--------------------------------------------------------------------------------------------------------------------------------------------------->


<div id="loadBtn" class="btn btn-primary" onclick="manageRegistrar();" >Reload Data/ Up-To-Date</div>

<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-primary" onclick="loadFile('addRegistrarTemplate.html');">Add Registrar/Deceased</button>


<p></p>

<div class="container" style="float:right">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Suggested Updates</h3>
                    <div class="pull-right">
                        <span class="clickable filter" data-toggle="tooltip" title="Toggle table filter" data-container="body">
                            <i class="glyphicon glyphicon-filter"></i>
                        </span>
                    </div>
                </div>

                <!--Table-->
                <table class="table table-striped">

                    <!--Table head-->
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>User Account</th>
                            <th>Deceased Id</th>
                            <th>Submitted By</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <!--Table head-->

                    <!--Table body-->
                    <tbody>
                        <tr>
                            <%
                                if (updated.size() > 0 && updated != null) {
                                    for (SuggestedUpdated su : updated) {
                            %>

                            <th scope="row"><%=su.getUpdateId()%></th>
                            <td><%=su.getUsers().getUserName()%></td>
                            <td><%=su.getRegId()%></td>
                            <td><%=su.getUsers().getFirstName()%></td>
                            <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mod">View Details</button>
                                <button type="button" class="btn btn-success">Accept</button>
                                <button type="button" class="btn btn-danger">Decline</button></td>
                        </tr>
                        <%}
                        } else {%><p> No Updates Logged</p>
                    <%}%>
                    </tbody>
                    <!--Table body-->
                </table>
                <!--Table-->
            </div>
            <div class="modal fade" id="mod" role="dialog">

                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">

                            <h4 class="modal-title">Record Details</h4>
                        </div>
                        <div class="modal-body">

                            <%
                                if (updated.size() > 0 && updated != null) {
                                    for (SuggestedUpdated su : updated) {
                            %>


                            <p><%=su.getRegInfo()%></p>
                            <%}
                                }%>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="myRegModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">

                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

</div>




<p></p>
<p></p>

<p></p>
<p></p>
<p>------</p>


