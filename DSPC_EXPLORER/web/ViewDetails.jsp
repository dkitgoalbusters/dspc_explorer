<%-- 
    Document   : ViewDetails
    Created on : 23-Apr-2018, 10:15:48
    Author     : abdul
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.stream.Collectors"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.dspc_explorer.Dtos.Registrar"%>
<%@page import="com.dspc_explorer.Dtos.Graveowner"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<% String data = (String) request.getSession().getAttribute("jsonStringUserList");
    Graveowner list = (Graveowner) request.getSession().getAttribute("graveowner");
%>






<div class="container"style="padding-left: 8rem; padding-right: 8rem;" >
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-primary">
                <div class="panel-heading">  <h4 >Record Details</h4></div>
                <div class="panel-body">

                    <div class="box box-info">

                        <div class="box-body">
                            <div class="col-sm-6">
                                <div  align="center"> <img src="images/r-i-p-grave-stone-halloween-decoration-34707618.jpg" style="height: 150px;" class="img-circle img-responsive" alt="profile image"/>  

                                    <!--    <input id="profile-image-upload" class="hidden" type="file">
                                        <div style="color:#999;" >click here to change profile image</div>
                                    <!--Upload Image Js And Css-->

                                </div>

                                <br>

                                <!-- /input-group -->
                            </div>
                            <div class="col-sm-6">
                                <h2 style="color:#00b1b1;">Grave Number #<%=list.getGraveId()%> Family Details </h2></span>

                            </div>
                            <div class="clearfix"></div>

                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#home">Person Details</a></li>
                                <li><a data-toggle="tab" href="#menu3">Grave Details</a></li>

                            </ul>



                            <div class="tab-content">
                                <div id="home" class="tab-pane fade in active">
                                    <%
                                        for (Iterator iterator1 = list.getRegistrars().iterator(); iterator1.hasNext();) {
                                            Registrar re = (Registrar) iterator1.next();
                                    %>  
                                    <div>

                                        <p></p>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>
                                        <div class="col-sm-5 col-xs-6 tital " ><strong>First Name:</strong></div><div class="col-sm-5 col-xs-6 "><%=re.getRegFirstName()%></div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 tital " ><strong>Middle Name:</strong></div><div class="col-sm-7"> <%=re.getRegMiddleName()%></div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 tital " ><strong>Last Name:</strong></div><div class="col-sm-7"> <%=re.getRegLastName()%></div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>
                                        
                                        <div class="col-sm-5 col-xs-6 tital " > <button type="button"  name="View" class="btn btn-danger btn-xs delete" onclick="viewRegistrar(<%=re.getRegId()%>)"> View Details</button></div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>
                                        
                                       


                                        <p>...</p>

                                        <%}%>


                                    </div>


                                    <div id="menu3" class="tab-pane fade">
                                        <h4>Grave Details</h4>
                                        <div class="col-sm-5 col-xs-6 tital " ><strong>Grave Owner Name:</strong></div><div class="col-sm-7"><%=list.getGraveOwnerName()%></div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 tital " ><strong>Grave Reference Code:</strong></div><div class="col-sm-7"><%=list.getGraveRefCode()%></div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 tital " ><strong>Grave Opened on:</strong></div><div class="col-sm-7"> <%=list.getGraveopenDate()%></div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 tital " >
                                            <button type="button"  name="View" class="btn btn-danger btn-xs delete" onclick="loadFile('maps.jsp')">View Location</button>
                                        </div>
                                    </div>



                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->

                            </div>


                        </div> 
                    </div>
                </div>  

            </div>
        </div>
    </div>




