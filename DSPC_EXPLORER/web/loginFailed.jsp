<%-- 
    Document   : loginFailed
    Created on : 14-Feb-2018, 07:35:46
    Author     : Abdul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="./css/login.css">
        <title>Login Failed</title>
    </head>
    <style>
        body.masthead-heading{
             font-size: 4rem;    
        }
        .textBox{
            padding-top: 250px;
            
        }

    </style>
    <body class="text-center text-white">
        <div class="container">
            <div class="textBox">
                <h1 class="masthead-heading mb-0">Oops Something went wrong</h1><br><br>
          
            <a href="log_in.jsp"  class="btn btn-primary btn-lg">Go back</a>
            </div>
        </div>
    </body>
</html>
