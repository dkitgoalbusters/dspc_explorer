/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dspc_explorer.Commands;

import com.dspc_explorer.Dtos.Users;
import com.dspc_explorer.services.UserServices;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import static org.apache.logging.log4j.web.WebLoggerContextUtils.getServletContext;

/**
 *
 * @author Abdul
 */
public class ProfileImageUploadCommand implements Command {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        String UPLOAD_DIRECTORY = request.getRealPath("/images/profilePictures/");
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        HttpSession session = request.getSession(true);
        String userIdString = request.getParameter("userToBeUpdated");
        int userId = -1;
        if (userIdString != null) {
            userId = Integer.parseInt(userIdString);
        }
        System.out.println(userId);
        UserServices userService = new UserServices();
        Users tmpUser = (Users) session.getAttribute("user");
        try {
            if (tmpUser != null && userId != tmpUser.getUserId() && userId != -1) {
                tmpUser = userService.getUserById(userId);
                if (tmpUser != null) {
                    String imageFileName = String.valueOf(tmpUser.getUserId());
                    // process only if its multipart content
                    if (isMultipart) {
                        // Create a factory for disk-based file items
                        FileItemFactory factory = new DiskFileItemFactory();

                        // Create a new file upload handler
                        ServletFileUpload upload = new ServletFileUpload(factory);

                        // Parse the request
                        List<FileItem> multiparts = upload.parseRequest(request);
                        for (FileItem item : multiparts) {
                            if (!item.isFormField()) {
                                String name = new File(item.getName()).getName();
                                String extension = FilenameUtils.getExtension(name);
                                System.out.println(extension);
                                String imgPathAndName = UPLOAD_DIRECTORY + File.separator + imageFileName + "." + extension;

                                item.write(new File(imgPathAndName));
                                tmpUser.setProfileImage(imageFileName + "." + extension);
                                userService.update(tmpUser);
                            }
                        }

                        // File uploaded successfully
                        session.setAttribute("status", 0);
                        session.setAttribute("statusMessage", "Your file has been uploaded!");
                        System.out.println("Uploaded");

                    } else {
                        session.setAttribute("status", 1);
                        session.setAttribute("statusMessage", "No Multi Part form to support file Upload");
                        System.out.println("No Multi Part form to support file Upload");
                    }
                } else {
                    session.setAttribute("status", 2);
                    session.setAttribute("statusMessage", "No Valid user logged in");
                    System.out.println("No Valid user logged in");
                }
            } else {
                session.setAttribute("status", 3);
                session.setAttribute("statusMessage", "Session Expired");
                System.out.println("Session Expired");
            }
            request.getRequestDispatcher("/ProcessResult.jsp").forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(DeleteUserCommand.class.getName()).log(Level.SEVERE, null, ex);
            session.setAttribute("status", 3);
            session.setAttribute("statusMessage", "File Upload Failed due to " + ex.getMessage());
            System.out.println("File Upload Failed due to" + ex.getMessage());
            RequestDispatcher dispatcher = request.getRequestDispatcher("/ProcessResult.jsp");
            try {
                dispatcher.forward(request, response);
            } catch (ServletException | IOException ex1) {
                Logger.getLogger(DeleteUserCommand.class.getName()).log(Level.SEVERE, null, ex1);
            }

        }

    }

}
