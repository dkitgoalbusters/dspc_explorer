/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dspc_explorer.Commands;

import com.dspc_explorer.Dtos.Users;
import com.dspc_explorer.services.UserServices;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static org.apache.logging.log4j.web.WebLoggerContextUtils.getServletContext;

/**
 *
 * @author Abdul
 */
public class ModifyUserCommand implements Command {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        UserServices userservice = new UserServices();
        HttpSession session = request.getSession();
        try {
            int userId = Integer.parseInt(request.getParameter("userId"));
            Users user = userservice.getUserById(userId);

            if (user != null) {
                session.setAttribute("modifyUser", user);
                session.setAttribute("status", 0);
                session.setAttribute("statusMessage", "user exist");
                RequestDispatcher dispatcher = request.getRequestDispatcher("/ModifyUser.jsp");
                dispatcher.forward(request, response);
            } else {
                session.setAttribute("status", 1);
                session.setAttribute("statusMessage", "find user Failed.. ");
                RequestDispatcher dispatcher = request.getRequestDispatcher("/ProcessResult.jsp");
                dispatcher.forward(request, response);
            }

        } catch (ServletException | IOException ex) {
            Logger.getLogger(ModifyUserCommand.class.getName()).log(Level.SEVERE, null, ex);
            session.setAttribute("status", 4);
            session.setAttribute("statusMessage", ex.getMessage());
            RequestDispatcher dispatcher = request.getRequestDispatcher("/ProcessResult.jsp");
            try {
                dispatcher.forward(request, response);
            } catch (ServletException | IOException ex1) {
                Logger.getLogger(ModifyUserCommand.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } catch (NullPointerException | NumberFormatException e) {
            session.setAttribute("status", 5);
            session.setAttribute("statusMessage", "Invalid User ID" + "<br>" + e.getMessage());
            RequestDispatcher dispatcher = request.getRequestDispatcher("/ProcessResult.jsp");
            try {
                dispatcher.forward(request, response);
            } catch (ServletException | IOException ex1) {
                Logger.getLogger(ModifyUserCommand.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
}
