/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dspc_explorer.Commands;

import com.dspc_explorer.services.UserServices;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author abdul
 */
public class CreateGraveOwnerCommand implements Command {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        try {
            int id = Integer.parseInt(request.getParameter("id"));
            String name = request.getParameter("name");
            String address = request.getParameter("address");
            String refCode = request.getParameter("graverefcode");
            String date = request.getParameter("dateopened");
            int row = Integer.parseInt(request.getParameter("row"));
            int dep = Integer.parseInt(request.getParameter("depth"));
            String gsize = request.getParameter("gsizes");
            SimpleDateFormat sm = new SimpleDateFormat("yyyy-mm-dd");
            Date dt = sm.parse(date);

            HttpSession session = request.getSession();

            UserServices userservices = new UserServices();
            RequestDispatcher dispatcher;
            
            if (userservices.createGraveOwner(id, name, address, refCode, dt, row, dep, gsize)) {
                session.setAttribute("status", 0);
                session.setAttribute("statusMessage", "Owner Created Sucessfully");
                dispatcher = request.getRequestDispatcher("/ProcessResult.jsp");
                dispatcher.forward(request, response);
            } else {
                session.setAttribute("status", 1);
                session.setAttribute("statusMessage", "Onwer Creation unSucessfull");
                 dispatcher = request.getRequestDispatcher("/ProcessResult.jsp");
                dispatcher.forward(request, response);
            }
        } catch (ParseException ex) {
            Logger.getLogger(CreateGraveOwnerCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(CreateGraveOwnerCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CreateGraveOwnerCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
