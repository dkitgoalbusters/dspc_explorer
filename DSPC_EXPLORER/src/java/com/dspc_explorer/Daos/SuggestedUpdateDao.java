/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dspc_explorer.Daos;

import com.dspc_explorer.Dtos.SuggestedUpdated;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author abdul
 */
public class SuggestedUpdateDao implements SuggestedUpdateDaoInterface {
    
    Session session = null;
    Transaction tx = null;

    /**
     * This is the user command class Create HibernateUtil session to gain
     * access to database
     */
    /**
     * User
     *
     * @param updated
     * @return true if insert statement is successful
     */
    @Override
    public boolean insertSuggeted(SuggestedUpdated updated){
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            if (session != null) {
                session.save(updated);
                tx.commit();
                return true;
            }
            session.close();
        } catch (HibernateException e) {
            try {
                tx.rollback();
            } catch (RuntimeException r) {
                System.out.println("Can't rollback transaction" + r);

            }
            throw e;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return false;
    }


    /**
     *
     * @param suggested
     * @return
     */
    @Override
    public boolean updateSuggested(SuggestedUpdated suggested) {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            if (session != null) {
                session.update(suggested);
                tx.commit();
                return true;
            }

        } catch (HibernateException e) {
            try {
                tx.rollback();
            } catch (RuntimeException r) {
                System.out.println("Can't rollback transaction");

            }
            throw e;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return false;
    }

    /**
     *
     * @return A list of users from the database.
     */
    @Override
    public List<SuggestedUpdated> getAllSuggestedUpdates() {

        try {
            session = HibernateUtil.getSessionFactory().openSession();

            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(SuggestedUpdated.class);
            criteria.setFetchMode("users", FetchMode.JOIN);
            
            List<SuggestedUpdated> userList = criteria.list();

            //SQLQuery query = session.createSQLQuery("call GetListUser()").addEntity(Users.class);
            //List<Users> userList = (ArrayList<Users>) query.list();
            tx.commit();
            return userList;
        } catch (HibernateException e) {
            try {
                tx.rollback();
            } catch (RuntimeException r) {
                System.out.println("Can't rollback transaction");

            }
            throw e;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     *
     * @param sugId
     * @return
     */
    @Override
    public boolean deleteUser(int sugId) {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            SuggestedUpdated updated = new SuggestedUpdated();
            updated.setUpdateId(sugId);

            if (session != null) {
                session.delete(updated);
                tx.commit();
                return true;
            }

        } catch (HibernateException e) {
            try {
                tx.rollback();
            } catch (RuntimeException r) {
                System.out.println("RunTime Exception at deleteUser() Method in UserDao(class):  Can't rollback transaction");

            }
            throw e;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return false;
    }

    @Override
    public SuggestedUpdated getUpdateById(int updateId) {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            // here get object
            Criteria criteria = session.createCriteria(SuggestedUpdated.class);
            criteria.setFetchMode("users", FetchMode.JOIN)
                    .setFetchMode("suggestedUpdateds", FetchMode.JOIN);            
            criteria.add(Restrictions.idEq(updateId));
            
            List<SuggestedUpdated> list = criteria.list();// get the list of result obtained by given criteria
            if (list != null && list.size() > 0) {
                return list.get(0);
            }
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            Logger.getLogger("con").log(Level.SEVERE, "Exception: {0}", ex.getMessage());
            ex.printStackTrace(System.err);
            System.out.println("Login Exception" + ex.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return null;
    }
    
}
