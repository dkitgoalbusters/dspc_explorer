/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dspc_explorer.Daos;

import com.dspc_explorer.Dtos.SuggestedUpdated;
import java.util.List;

/**
 *
 * @author abdul
 */
public interface SuggestedUpdateDaoInterface {
    public boolean insertSuggeted(SuggestedUpdated updated);
    public boolean updateSuggested(SuggestedUpdated suggested);
    public List<SuggestedUpdated> getAllSuggestedUpdates();
    public boolean deleteUser(int sugId);
    public SuggestedUpdated getUpdateById(int updateId);
    
}
